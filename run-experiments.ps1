$algos = "AscentNaive", "Naive", "AscentOpt", "DatafrogOpt"
$progs = '.\inputs\clap-rs\app-parser-{{impl}}-add_defaults\',
         '.\inputs\private-ser-content-{impl#14}-fmt',
         '..\ascent\ascent_macro\nll-facts\ascent_codegen-compile_mir_rule\',
         '.\nll-facts\output-ascent_naive-compute', 
         '..\chess\nll-facts\chess_engine-{impl#3}-analyze_core'

foreach ($prog in $progs) {
    Write-Output "=======================$($prog)========================" 
    foreach ($algo in $algos) {
        cargo  run --release -- --show-tuples -a $algo $prog
    }
    Write-Output "==========================================================================="
    Write-Output "" 

}

# cargo  run --release -- --show-tuples -a $algo '.\nll-facts\output-ascent_naive-compute'
# cargo  run --release -- --show-tuples -a $algo '.\inputs\clap-rs\app-parser-{{impl}}-add_defaults\'
#cargo  run --release -- --show-tuples -a $algo '..\ascent\ascent_macro\nll-facts\ascent_codegen-compile_mir_rule\'
#cargo  run --release -- --show-tuples -a $algo '..\chess\nll-facts\chess_engine-{impl#3}-analyze_core'

