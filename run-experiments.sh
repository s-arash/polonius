algos=("AscentNaive" "Naive" "AscentOpt" "DatafrogOpt")
progs=('./inputs/clap-rs/app-parser-{{impl}}-add_defaults' \
       './inputs/private-ser-content-{impl#14}-fmt' \
       './inputs/ascent_codegen-compile_mir_rule' \
       './inputs/output-ascent_naive-compute' \
       './inputs/chess_engine-{impl#3}-analyze_core')

echo "Note: most experiments take a substantial amount of time to read the input facts from disk."
echo "The time we measure does not include that, it only includes the analysis time."

echo "Building Polonius ..."
cargo build --release
if [ $? -ne 0 ] 
then 
  echo "Build failed" 
  exit 1
fi

for prog in ${progs[@]}; do
    echo "=======================$prog========================" 
    for algo in ${algos[@]}; do
        echo "=== $algo:"
        ./target/release/polonius --skip-timing -a $algo $prog
        echo ""
    done
    echo ""
    echo "" 
done